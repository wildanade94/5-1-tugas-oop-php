<?php
// buat class hewan
class hewan {
  
   // buat property untuk class hewan
   var $nama;
   var $darah;
   var $jumlahkaki;
   var $keahlian;
  
   // buat method untuk class atraksi
   function atraksi($nama, $keahlian) {
     return $nama . " Sedang " . $keahlian;
    }
}
  
// buat objek dari class hewan (instansiasi)
$hewan_1 = new hewan();
$hewan_2 = new hewan();
  
// set property
$hewan_1->nama="Elang";
$hewan_1->keahlian="Terbang Tinggi";
$hewan_2->nama="Harimau";
$hewan_2->keahlian="Lari Cepat";
  
// tampilkan method
echo $hewan_1->atraksi($hewan_1->nama,$hewan_1->keahlian);
echo "<br />";
echo $hewan_2->atraksi($hewan_2->nama,$hewan_2->keahlian);
echo "<br />";
echo "<br />";

class fight {
  
    // buat property untuk class fight
    var $nama1;
    var $nama2;
    var $attackpower;
    var $defencepower;
   
    // buat method untuk class serang
     function serang($nama1,$nama2) {
        return $nama1 . " Sedang menyerang " . $nama2;
     }

     function diserang($nama1,$nama2){
        return $nama1 . " Sedang diserang " . $nama2;
     }
 }

// buat objek dari class fight (instansiasi)
$hewan_3 = new fight();
$hewan_4 = new fight();
  
// set property
$hewan_3->nama1="Elang";
$hewan_3->nama2="Harimau";
$hewan_4->nama1="Harimau";
$hewan_4->nama2="Elang";
  
// tampilkan method
echo $hewan_3->serang($hewan_3->nama1,$hewan_3->nama2);
echo "<br />";
echo $hewan_4->serang($hewan_4->nama1,$hewan_4->nama2);
echo "<br />";
echo "<br />";
echo $hewan_3->diserang($hewan_3->nama1,$hewan_3->nama2);
echo "<br />";
echo $hewan_4->diserang($hewan_4->nama1,$hewan_4->nama2);
?>
